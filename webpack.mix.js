const { mix } = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/js/compilers/app.js', 'public/js')
	.js('resources/assets/js/compilers/public_event_list.js', 'public/js')
	.js('resources/assets/js/compilers/create_event.js', 'public/js')
	.js('resources/assets/js/compilers/promotor_event_list.js', 'public/js')
	.js('resources/assets/js/compilers/following_event_list.js', 'public/js')
	.js('resources/assets/js/compilers/edit_event.js', 'public/js')

	.js('resources/assets/js/compilers/cancel_event.js', 'public/js')
	.js('resources/assets/js/compilers/google_maps.js', 'public/js')
	.js('resources/assets/js/compilers/admin_dashboard.js', 'public/js')
	.js('resources/assets/js/compilers/register_user.js', 'public/js')
	.js('resources/assets/js/compilers/promotor_ticket_event_list.js', 'public/js')
	.js('resources/assets/js/compilers/orders_list.js', 'public/js')

	.js('resources/assets/js/compilers/event_details.js', 'public/js')
	.js('resources/assets/js/compilers/promotor_stats.js', 'public/js')
	.js('resources/assets/js/compilers/user_profile.js', 'public/js')
	.js('resources/assets/js/compilers/featured-event.js', 'public/js')
	.js('resources/assets/js/compilers/sell_tickets.js', 'public/js')
	.js('resources/assets/js/compilers/tickets_graph.js', 'public/js')
	.js('resources/assets/js/compilers/event_entrances.js', 'public/js')
	.js('resources/assets/js/compilers/change_password.js', 'public/js');

	//.copy('node_modules/autosize/dist/autosize.min.js', 'public/js')
	//.copy('./node_modules/alertifyjs/build/css/alertify.min.css', './public/css')

	//.copy('./node_modules/alertifyjs/build/alertify.min.js', './public/js')
	
	//.sass('./resources/assets/sass/app.scss', './public/css')
	//.less('./node_modules/bootstrap-datepicker/build/build_standalone.less', './public/css');
