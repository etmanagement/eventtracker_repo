 require('./vue_bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('event-list-wrapper', require('../components/users/event-list-wrapper.vue'));

Vue.component('event-list', require('../components/events/event-list.vue'));

Vue.component('following-event', require('../components/users/following-event.vue'));

Vue.component('event', require('../components/events/event.vue'));

 const app = new Vue({
 	el: '#vue'

 });
