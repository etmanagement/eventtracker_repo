'use strict';

class Paginator {

      constructor(){

          this.currentPage = 1;
          this.eventCount = 1;
          this.lastPage = 1;
          this.displayedPages = [1,2,3,4,5];
  }


  update(currentPage, eventCount, lastPage)
  {
       this.currentPage = currentPage;
       this.eventCount = eventCount;
       this.lastPage = lastPage;
       this.displayedPages = [];


       var cicles = lastPage > 5 ? 5 : lastPage;
       var firstPage = 1
       var finalPage = lastPage;


        	// Temos mais de 5 páginas ou 5 páginas
        	if(cicles == 5)
        	{	
        		//O finalPage não pode exceder o lastPage
        		if (currentPage + 2 >= lastPage ) {
        			finalPage = lastPage;
        		}
        		else {
        			if ( currentPage - 2 < 1) {
        				finalPage = 5;
        			}
        			else {
        				finalPage = currentPage + 2;
        			}
        		}

        		//O firstPage não pode ser menos do que 1
        		if(currentPage - 2 <= 1)
        		{
        			firstPage = 1;
        		}
        		else{
        			//Temos uma situação em que o limite superior poderá ser o lastPage ou current + 1
        			if(currentPage + 2 > lastPage)
        			{
        				firstPage = (( currentPage - 2 ) - ( currentPage + 2 - lastPage ));
        			}
        			//Não se verifica
        			else{
        				firstPage = currentPage - 2;
        			}
        		}
        	}

        	for(let i = firstPage; i <= finalPage; i ++){
        		this.displayedPages.push(i);
        	}


        }
};

module.exports = Paginator;