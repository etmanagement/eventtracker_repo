@extends('promotor.sidebar')

@section('contentPromotor')
<div class="row">
	<div class="col-xs-12">
		<div id="vue" class="panel panel-body">
			<event-entrances encoded="{{ json_encode($ticketsData) }}"></event-entrances>
		</div>
	</div>
</div>

<script src="{{URL::asset('/js/event_entrances.js')}}"></script>
@endsection