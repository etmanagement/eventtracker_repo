@extends('promotor.sidebar')

@section('contentPromotor')
<link href="{{ URL::asset('/css/event-tickets.css') }}" rel="stylesheet">
<div class="content">	
	<div class="panel panel-body">
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<img src="/images/event/list/{{$event->image_id}}" class="img">
		</div>
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
			<h3>{{ $event->name }}</h3>
			<p>
				<b> De </b> {{ $event->getDate($event->initDate)}} <b> <br>Até </b> {{ $event->getDate($event->finishDate)}}

			</p>
			<p>
				<b>Distrito:</b> {{ $event->district->name }}
			</p>
			<p>
				<div class="col-xs-12 noPadding">
					<div class="col-xs-9 noPadding">
						<b>Categoria:</b> {{ $event->category->categoryName }}
					</div>
					<div class="col-xs-3 noPadding">
						<a href="{{ url('/event', $event->id) }}" class="button is-medium is-primary">Ver Detalhes</a>
					</div>	
				</div>
				
			</p>
		</div>

	</div>
	<div id=vue>
		<div class="panel panel-body">
			<tickets-graph tickets=" {{ json_encode($ticketsData) }} "></tickets-graph>
		</div>
	</div>

	<div class="panel panel-body">
		<h3>Tipos de Bilhetes</h3>

		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Preço</th>
						<th>Vendidos</th>
						<th>Disponveis</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($event->ticketTypes as $ticket_type)
					<tr>
						<td>{{ $ticket_type->name }}</td>
						<td>{{ $ticket_type->price }}€</td>
						<td>{{ count($ticket_type->tickets) }}</td>
						<td>{{ ($ticket_type->quantity-count($ticket_type->tickets)) }}</td>
						<td>{{ $ticket_type->quantity }}</td>
						@endforeach
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="{{ URL::asset('/js/tickets_graph.js') }}"></script>
@endsection