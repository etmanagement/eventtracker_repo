@extends('promotor.sidebar')

@section('contentPromotor')
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="vue">
			<event-list-wrapper base_url="/promotorView/ticketmanagement"></event-list-wrapper>
		</div>
	</div>
</div>

<script src="{{URL::asset('/js/promotor_ticket_event_list.js')}}"></script>
@endsection