@extends('promotor.sidebar')

@section('contentPromotor')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">
<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>

<div class="row">
	<div id="vue" class="col-md-12">
		<event-cancel event_data="{{json_encode($event)}}"></event-cancel>
	</div>
</div>

<script src="{{ URL::asset('/js/cancel_event.js') }}"></script>
@endsection