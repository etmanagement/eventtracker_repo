@extends('layouts.app')

@section('content')
<link href="{{ URL::asset('/css/report-event.css') }}" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Denunciar Evento</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/event/report', $event->id) }}">
						{{ csrf_field() }}

						<div class="form-group paddingTop">
							<label class="col-md-4 control-label">Nome do Evento</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->name }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Nome do Promotor</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->user->name }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Distrito</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->district->name }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Data de Inicio</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->initDate }}</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Data de Fim</label>

							<div class="col-md-6">
								<label class="control-label">{{ $event->finishDate }}</label>
							</div>
						</div>

						<div class="form-group{{ $errors->has('reason') ? ' has-error' : '' }}">
							<label for="reason" class="col-md-4 control-label">Razão da Denúncia</label>

							<div class="col-md-6">
								<textarea id="reason" class="form-control" name="reason" placeholder="Insira aqui a razão da denúncia!" value="{{ old('reason') }}" required></textarea>

								@if ($errors->has('reason'))
								<span class="help-block">
									<strong>{{ $errors->first('reason') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<hr>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4 inline">
								<button type="submit" class="button is-large is-info">
									Denunciar
								</button>
								<a href="{{ url('/event', $event->id) }}" class="button is-large is-danger">Cancelar</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection