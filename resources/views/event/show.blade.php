@extends('layouts.app')

@section('content')
<link href="{{ URL::asset('/css/show-event.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">


<div class="row">
	<div class="col-xs-12 col-md-10 col-md-offset-1">
		@if ($success = Session::get('success'))
		<div align="center" class="alert alert-success">
			<b>{{$success}}</b>
		</div>
		@endif

		@if ($error = Session::get('error'))
		<div align="center" class="alert alert-danger">
			<b>{{$error}}</b>
		</div>
		@endif
		<div class="panel panel-body">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<div class="image">
					<img src="{{ url('/images/event', $event->image_id ) }}">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h2 align="center">{{ $event->name }}</h2><br>
				<h5 class="noPadding col-sm-3 alignRight" ><b>Inicio:</b></h5>
				<h5 class="noPadding col-sm-9"> {{ $event->getDate($event->initDate) }}</h5>
				<h5 class="noPadding col-sm-3 alignRight"><b>Fim:</b></h5>
				<h5 class="noPadding col-sm-9"> {{ $event->getDate($event->finishDate) }}</h5>
				@if(isset($event->spaceName))
				<h5 class="noPadding col-sm-3 alignRight"><b>Espaço:</b></h5>
				<h5 class="noPadding col-sm-9"> {{ $event->spaceName  }}</h5>
				@endif
				@if(isset($event->address))
				<h5 class="noPadding col-sm-3 alignRight"><b>Morada:</b></h5>
				<h5 class="noPadding col-sm-9"> {{ $event->address }}</h5>
				@endif
				<h5 class="noPadding col-sm-3 alignRight"><b>Categoria:</b></h5>
				<h5 class="noPadding col-sm-9"> {{ isset($event->category) ? $event->category->categoryName : "A definir" }}</h5>
				<h5 class="noPadding col-sm-3 alignRight"><b>Etiquetas:</b></h5>
				<h5 class="noPadding col-sm-9">
					@foreach ($event->tags as $tag)
					#{{ $tag->tag }}
					@endforeach
				</h5>
			</div>
			<br>
			<div id="vue">
				<event-options event_id="{{ json_encode($event->id) }}" event_owner="{{json_encode($event->user) }}" is_guest="{{ json_encode(Auth::guest()) }}" is_following="{{ json_encode($event->is_following) }}" in_evaluation="{{ json_encode($event->inEval) }}" is_owner="{{ json_encode( (Auth::guest() ? false : (Auth::user()->id == $event->user_id) ? true : false)) }}" eval_type="{{ json_encode($event->evalType) }}"></event-options>
			</div>

			<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
				<hr>
				<div class="col-md-6" align="center">
					<h4>{{ $event->counter_views }}</h4>
					<h5>Visualizações</h5>

				</div>
				<div class="col-md-6" align="center">
					<h4>{{ count($event->followers) }}</h4>
					<h5>Seguidores</h5>
					@if(count($event->followers)>0 && !Auth::guest())
					@if(Auth::user()->id == $event->user_id)
					<a href="{{ url('/event/'.$event->id.'/sendmessage') }}" class="button is-primary is-medium col-md-offset-1 col-sm-offset-1 col-md-10 col-sm-10 col-xs-12">Enviar Mensagem</a>
					@endif
					@endif
				</div>
			</div>
			@if($event->status == 2)
			<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
				<hr>
				<div class="alert alert-danger">

					<h4><b><u>AVISO DE CANCELAMENTO</u></b></h4>
					<h5><b>Este evento foi cancelado pelo seu promotor com o seguinte aviso:</b></h5>
					<p>{{$event->cancellation->reason}}</p> 
				</div>	
			</div>


			@endif
			@if(count($event->multimediaResources)>0)

			<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
				<hr>
				<h4><b>Multimedia</b></h4>

				@foreach($event->multimediaResources as $multimedia)
				<div class="col-sm-offset-1 col-sm-5 col-xs-10 col-xs-offset-1">
					{{ $multimedia->name }}
				</div>
				<div class="col-sm-4 col-sm-offset-2 col-xs-10 col-xs-offset-1">
					<a href="http:\\{{ $multimedia->url }}">{{ $multimedia->url}}</a>
				</div>
				<br>
				@endforeach

			</div>
			@endif
			@if($event->hasTickets)

			<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">


				@if($event->ticketManagement == 'external')
				<hr>			
				<h4><b>Bilheteira</b></h4>
				<div class="col-md-12">
					<h5><b>Quantidade Disponivel à Partida:</b></h5>{{$event->ticketsForSale}}

				</div>
				@elseif(!$event->initDateHasPassed())
				<div class="col-md-12">
					<hr>			
					<h4><b>Bilheteira</b></h4>

					<p align="center" class="col-xs-12"><b>Bilhetes a partir de {{ $event->ticketsStartingAt()}}€</b>
						<br>
						Bilhetes Vendidos Através do Sistema de Bilheteira EventTracker</p>
						@if(!Auth::guest())

						@if(Auth::user()->id == $event->user_id)
						<a  class="col-xs-offset-1 col-xs-10 col-sm-4 col-sm-offset-4 button is-medium is-success" href="{{ url('/event/'.$event->id.'/tickets') }}">Consultar Vendas</a>
						@endif
						@endif
						@if($event->isOwner(Auth::user()) && $event->initDate < Carbon::now())
						<a  class="col-xs-offset-1 col-xs-10 col-sm-4 col-sm-offset-4 button is-medium is-success" href="{{ url('/ticketing/event/'.$event->id) }}">Adquirir Bilhetes</a>
						@endif
					</div>
					@endif
					@if(count($event->ticketSalesPoints)>0)
					<div class="col-md-12">


						<h4><b>Pontos de Aquisição</b>	</h4>

						@foreach($event->ticketSalesPoints as $key => $ticketPoint)
						<div class="col-md-10">
							<h5><b>Ponto de venda #{{$key+1}}:</b></h5>
							<a href="http:\\{{$ticketPoint->url}}">{{ $ticketPoint->url}}</a>
						</div>
						<br>
						@endforeach
						@if($event->ticketSaleDescription)
						<div class="col-md-12">
							<h5><b>Informações Adicionais</b></h5>{{$event->ticketSaleDescription}}
						</div>
						@endif
					</div>
					@endif
				</div>
				@endif

				<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
					<hr>
					<h4><b>Descrição</b></h4>
					<div class="col-md-10">
						{{ $event->description }}
					</div>
					<br>
					<hr>
					<h4><b>Localização no Mapa</b></h4>
					<br>
					<div id="gmaps-vue">
						<google-maps-wrapper lat="{{json_encode($event->lat)}}" lng="{{json_encode($event->lng)}}" editable="false">
						</google-maps-wrapper>
					</div>
				</div>
			</div>
			@if(!Auth::guest())
			@if($event->status == 4 && $event->user_id == Auth::user()->id)
			<div class="col-xs-12 panel panel-body">
				<div class="col-xs-12">
					<h4><b>Avaliações</b></h4>
					@if(count($evaluations)==0)
					<p>Ainda não foram efetuadas Avaliações!</p>
					@else
					<div class="col-xs-12 inline">
						<p>Já foram realizadas algumas avaliações!</p>
						<a href="{{ url('event/'.$event->id.'/evaluations') }}" class="button is-medium is-info">Visualizar</a>
					</div>
					@endif
				</div>
			</div>
			@endif
			@endif
		</div>
	</div>
</div>

<script src="{{ URL::asset('/js/event_details.js') }}"></script>
@endsection