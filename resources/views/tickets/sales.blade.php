@extends('layouts.app')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">
<script src="https://use.fontawesome.com/1cf3fe9508.js"></script>
<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>

<div id="vue">
	<sell-tickets encoded="{{json_encode($event)}}" userflag="{{json_encode(!Auth::guest())}}" ></sell-tickets>
	
</div>

<script src="{{URL::asset('/js/sell_tickets.js')}}"></script>

@endsection