@extends('layouts.app')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap-datetimepicker-build.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">
<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>
<script src="https://use.fontawesome.com/1cf3fe9508.js"></script>
<div class="container">
	<div id="vue" class="row">
		<register-user districts="{{json_encode($districts)}}"></register-user>
	</div>
</div>
<script type="text/javascript" src="{{ URL::asset('/js/register_user.js') }}"></script>
@endsection
