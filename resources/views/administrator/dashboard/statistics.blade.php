@extends('administrator.dashboard.dashboard')

@section('contentDashboard')

<div id="vue" class="row">
	<dashboard events="{{json_encode($eventsData)}}" users="{{json_encode($usersData)}}" categories="{{json_encode($categoriesData)}}" events_monthly="{{json_encode($eventsThisYearData)}}" statistics="{{json_encode($otherStatistics)}}"></dashboard>
</div>
<script src="{{ URL::asset('/js/admin_dashboard.js') }}"></script>
@endsection