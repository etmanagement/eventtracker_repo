<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            @if (!Auth::guest())
            @if (Auth::user()->isAdmin() && strpos(url()->current(), 'admin') != false)
            <button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
                <b>MENU</b>
            </button>
            @endif
            @endif
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{{ url('/') }}">
                <b>{{ config('app.name', 'Event Tracker') }}</b>
            </a>
        </div>
        <div class="navbar-collapse collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (strpos(Request::url(), '/') != false)
                <li>
                    <a href="{{ url('/') }}"><b>Página Inicial</b></a>
                </li>
                @endif

                <li>
                    <a href="{{ url('/events') }}"><b>Eventos</b></a>
                </li>

                @if (Auth::guest())
                <li>
                    <a href="{{ route('login') }}"><b>Entrar | Registar</b></a>
                </li>
                @else
                @if (Auth::user()->isAdmin())
                <li>
                    <a href="{{ url('/admin/dashboard') }}"><b>Administração</b></a>
                </li>
                @else
                <li>
                    <a href="{{ url('/promotorView/advertised')}}"><b>Promoção de Eventos</b></a>
                </li>
                @endif
                
                @include('partials.userArea')
                @endif
            </ul>
        </div>

    </div>
</nav>
