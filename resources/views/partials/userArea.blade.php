<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <b>{{ Auth::user()->name }}</b> <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
    @if (!Auth::user()->isAdmin())
        <li>
            <a href="{{ url('user/personal/profile')}}"><b>Área Pessoal</b></a>
        </li>
        @endif
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <b>Logout</b>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>

</ul>
</li>