@extends('layouts.app')

@section('content')
<link href="{{ URL::asset('/css/promotor-area.css') }}" rel="stylesheet">
<div class="col-md-3 col-sm-3 col-xs-12">
	<div class="row profile">

		<div class="profile-sidebar">
			<!-- SIDEBAR USERPIC -->
			<div class="profile-userpic">
				<img src="{{ url('/images/profile', Auth::user()->image_id) }}" class="img-responsive" alt="">
			</div>
			<!-- END SIDEBAR USERPIC -->
			<!-- SIDEBAR USER TITLE -->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">

				</div>
				<div class="profile-usertitle-job">
					{{Auth::user()->name}}
				</div>
			</div>
			<!-- END SIDEBAR USER TITLE -->
			<!-- SIDEBAR BUTTONS -->
			<!-- END SIDEBAR BUTTONS -->
			<!-- SIDEBAR MENU -->
			<div class="profile-usermenu">
				<ul class="nav">
					<li class="{{ $currentView == 'profile' ? 'active' : '' }}">
						<a href="{{ url('/user/personal/profile') }}">
							Perfil Pessoal
						</a>
					</li>
					<li class="{{ $currentView == 'following' ? 'active' : '' }}">
						<a href="{{url('/user/personal/following')}}">
							Eventos A Seguir
						</a>
					</li>

					<li class="{{ $currentView == 'orders' ? 'active' : '' }}">
						<a href="{{url('/user/personal/orders')}}">
							Bilhetes Reservados

						</a>
					</li>
					@if(!Auth::user()->social_account)
					<li class="{{ $currentView == 'changePassword' ? 'active' : '' }}">
						<a href="{{url('/user/personal/changePassword')}}">
							Alterar Palavra Passe
						</a>
					</li>
					@endif
				</ul>
			</div>
			<!-- END MENU -->
		</div>
	</div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
	@yield('contentUser')
</div>
@endsection