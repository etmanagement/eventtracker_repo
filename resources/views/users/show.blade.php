@extends('layouts.app')

@section('content')
<link href="{{ URL::asset('/css/user-profile.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">
<div class="row">
	<div class="col-md-12 col-sm-12 col-md-offset-0 col-xs-12 paddingTop">
		<div class="panel panel-body">
			<div class="row">
				<div class="col-md-7 col-sm-7 col-xs-12">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h2 align="center">{{ $user->name }}</h2>
						<br>
					</div>
					@if($user->birthdate)
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label class="col-md-4" align="right">Data de Nascimento:</label>
						<div class="col-md-8"> 
							<p>{{ $user->dateToString() }}</p>
						</div>
					</div>
					@endif
					@if($user->gender)
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label class="col-md-4" align="right">Genero:</label> 
						<div class="col-md-8"> 
							<p>{{ $user->genderToStr() }}</p>
						</div>
					</div>
					@endif
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label class="col-md-4" align="right">Email:</label> 
						<div class="col-md-8">
							<p>{{ $user->email }}</p>
						</div>
					</div>
					@if($user->district->id != 1)
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label class="col-md-4" align="right">Distrito:</label> 
						<div class="col-md-8">
							<p>{{ $user->district->name }}</p>
						</div>
					</div>
					@endif
					@if($user->address)
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label class="col-md-4" align="right">Morada:</label> 
						<div class="col-md-8">
							<p>{{ $user->address }}</p>
						</div>
					</div>
					@endif
				</div>
				<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="{{ url('/images/profile', $user->image_id ) }}" class="img-responsive alignRight"/>
					@if(!Auth::guest())
					@if(Auth::user()->isAdmin())
					<div class="paddingTop" align="right">
						@if (!$user->blocked)
						<button name="{{$user->id}}" class="blockUser button is-medium is-danger">Bloquear</button>

						<form class="inline" id="blockUser-form-{{$user->id}}" action="{{ url('admin/user/toogleBlock/'.$user->id) }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
						@else

						<form class="inline" action="{{ url('admin/user/toogleBlock/'.$user->id) }}" method="POST">
							{{ csrf_field() }}
							<button type="submit" name="{{$user->id}}" class="button is-medium is-success">Desbloquear</button>
						</form>
						@endif
					</div>
					@endif
					@endif
				</div>
			</div>
			@if($user->description)
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<hr>
					<label class="col-md-12">Descrição</label>
					<div class="col-md-12">
						<p>{{ $user->description }}</p> 
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>
<script src="{{ URL::asset('/js/administrator.js') }}"></script>
@endsection