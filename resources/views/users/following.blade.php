@extends('users.sidebar')

@section('contentUser')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/alertify.min.css') }}">
<script type="text/javascript" src="{{ URL::asset('/js/alertify.min.js') }}"></script>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="vue">
			<event-list-wrapper base_url="/user/personal/following"></event-list-wrapper>
		</div>
	</div>
</div>

<script src="{{ URL::asset('/js/following_event_list.js') }}"></script>
@endsection