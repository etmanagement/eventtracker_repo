@extends('layouts.app')

@section('content')
<link href="./css/home.css" rel="stylesheet">
<div class="container ">
    <div class="row col-md-12 backgroundImg">

        <div id="vue">
            <featured-event encoded={{json_encode(Auth::guest() ? null : Auth::user()->id)}}></featured-event>
        </div>
    </div>
    <div class="row col-md-12 otherEvents">
        <div class="row col-md-12 otherEvents">
            @foreach($events as $key => $event)
            @if($key == 0 || $key == 2)
            <div class="row">
                @endif
                <div class="col-md-4 cardsPadding">
                    <div class="card">
                        <div class="card-image">
                            <figure class="image is-4by3">
                                <img class="img-responsive" src="{{ $event->image }}" alt="Image">
                            </figure>
                        </div>
                        <div class="card-content" align="center">
                            <p>{{$event->name}}</p>
                            <p>{{$event->getDate($event->initDate)}}</p>
                            <p>{{$event->district_id->name}}</p>
                        </div>
                        <footer class="card-footer">
                            <a href="{{ url('/event/'.$event->id) }}" class="card-footer-item button is-success is-large">Ver Mais Detalhes</a>
                        </footer>
                    </div>
                </div>
                @if($key == 2 || $key == 5)
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
<script src="{{ URL::asset('/js/featured-event.js') }}"></script>
@endsection
