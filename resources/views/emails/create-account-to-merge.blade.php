@component('mail::message')
Criação de conta associada a uma rede social!

Pressione o seguinte butão para acabar a criação de uma conta na nossa plataforma!

@component('mail::button', ['url' => $url])
Finalizar Conta
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
