@component('mail::message')
Confirmação do Email

Olá,

Falta apenas confirmar o seu email.

@component('mail::button', ['url' => $url])
Confirmar Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
