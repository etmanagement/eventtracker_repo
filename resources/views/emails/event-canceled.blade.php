@component('mail::message')
Caro/a {{ $user->name }},

Lamentamos informar que o evento {{ $event->name }}, do qual é seguidor, foi cancelado!

O promotor procedeu ao cancelamento apresentando o seguinte motivo,
@component('mail::panel')
{{$message}}
@endcomponent

A razão do cancelamento poderá também ser visualizada na página do evento,
@component('mail::button', ['url' => 'eventtracker.app/event/'.$event->id])
Visualizar Evento
@endcomponent

Lamentamos o sucedido,<br>
Equipa {{ config('app.name') }}
@endcomponent
