@component('mail::message')
Caro/a {{ $user->name }},

Informamos que o evento {{$event->name}}, do qual é seguidor, sofreu alterações por parte do promotor.

Poderá visualizar as alterações ao evento na página deste,
@component('mail::button', ['url' => 'eventtracker.app/event/'.$event->id])
Vizualizar Evento
@endcomponent

Pedimos desculpa pelo incómodo,<br>
Equipa {{ config('app.name') }}
@endcomponent
