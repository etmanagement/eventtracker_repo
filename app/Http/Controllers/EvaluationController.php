<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Evaluation;
use App\Event;
use App\UserEvaluation;


class EvaluationController extends Controller
{

	/** Evaluate an Event **/

	public function evaluate ($id)
	{
		$event = Event::findOrFail($id);

		return view('event.evaluate', compact('event'));
	}

	public function storeEvaluation (Request $request, $id)
	{
		$event = Event::findOrFail($id);

		$this->validator($request->all())->validate();

		$this->create($request->all(), $event->id, $event->user_id);

		if(!Auth::guest()){
			$this->createUserEvaluation($event->id, Auth::user()->id);
		}

		return redirect('/event/'.$id)->with('success', 'Evento avaliado com sucesso!');
	}


        /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
        protected function validator(array $data)
        {   
        	$rules = array (
        		'ratingE' => 'required',
        		'ratingP' => 'required',
        		'promoOpinion' => 'nullable|max:1000',
        		'eventOpinion' => 'nullable|max:1000');

        	$messages = array (
        		'ratingE.required' => 'É necessário fornecer uma avaliaçao ao evento em questão.',
        		'ratingP.required' => 'É necessário fornecer uma avaliaçao ao promotor do evento.',
        		'max' => 'A opinião fornececida não pode ter mais de :max carateres',
        		);

        	return Validator::make($data, $rules, $messages);
        }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data, $eventId, $promotorId)
    {   	
    	return Evaluation::create([
    		'event_id' => $eventId,
    		'promotor_id' => $promotorId,
    		'eventEval' => $data['ratingE'],
    		'eventOpinion' => $data['eventOpinion'],
    		'promotorEval' => $data['ratingP'],
    		'promotorOpinion' => $data['promoOpinion'],
    		]);
    }

    protected function createUserEvaluation($eventId, $userId)
    {
    	return UserEvaluation::create([
    		'event_id' => $eventId,
    		'user_id' => $userId,
    		]);
    }

    public function eventEvaluations($id)
    {
        $event = Event::findOrFail($id);
        $evaluations = $event->evaluations()->where('eventOpinion', '!=', '')
                                 ->orWhere('promotorOpinion', '!=', '')
                                 ->where('event_id', $id)->paginate(10);

        if($event->evaluations()->count()>0) {

            $promotorEval = round(Evaluation::where('event_id', $id)->sum('promotorEval')/Evaluation::where('event_id', $id)->count());

            $eventEval = round(Evaluation::where('event_id', $id)->sum('eventEval')/Evaluation::where('event_id', $id)->count());

        } else {
            $promotorEval = 0;
            $eventEval = 0;
        }

        return view('event.evaluations', compact('evaluations', 'event', 'promotorEval', 'eventEval'));
    }
}
