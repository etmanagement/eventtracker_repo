<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\SocialAccountService;

use Socialite;

class GoogleAuthController extends SocialAuthController
{
	const PROVIDER = 'google';

    public function callback(Request $request, SocialAccountService $service)
    {   

    	if($request->has('error'))
    	{
    		return redirect()->to('/login');
    	}
    	
        $user = $service->createOrGetUser(Socialite::driver(self::PROVIDER)->user(), self::PROVIDER);

        auth()->login($user);

        return redirect()->to('/');
    }
}
