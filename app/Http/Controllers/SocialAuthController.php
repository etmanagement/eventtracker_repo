<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Mail\FacebookAccountWithoutMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Socialite;
use App\User;
use App\FacebookTempAccount;

class SocialAuthController extends Controller
{
    public function redirect($driver)
    {
        return Socialite::driver($driver)->redirect();   
    }
    public function setFacebookEmail($id) 
    {
        return view("auth.register-email", compact('id'));
    }

    public function saveFacebookEmail(Request $request, $id)
    {
        $validator = $this->validateEmail($request->all());

        if(!$request['email'] || $validator->fails()) {
            return redirect()->back()->with("error", "O email inserido não é válido!");
        }
        else {
            $user = FacebookTempAccount::findOrFail($id);

            $user->given_email = $request['email'];
            $user->email_token = str_random(25);
            $user->save();

            Mail::to($user->given_email)->send(new FacebookAccountWithoutMail($user));

            return redirect('/login')->with("messageS", "Enviamos-lhe um email de confirmação! Por favor verifique o seu email!");
        }
    }

    public function validateEmail(array $data)
    {
        $rules = array (
            'email' => 'required|email|max:255|unique:users',
            );

        $messages = array (
            'email.unique' => 'O email fornecido já se encontra em utilização',
            'required' => 'O campo :attribute é de preenchimento obrigatório',
            'email' => 'O email fornecido terá de ter um formato válido',
            );

        return Validator::make($data, $rules, $messages);
    }
}