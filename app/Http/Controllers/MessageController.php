<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Validator;
use App\Jobs\ProcessNotifyFollowersOnMessage;

class MessageController extends Controller
{
	/*** Emails generated through events  **/

	public function showSendMailToFollowers($id)
	{
		$currentView = '';

		$event = Event::findOrFail($id);

		return view('event.send-message', compact('event', 'currentView'));
	}


	public function sendMailToFollowers(Request $request, $id)
	{
		$event = Event::findOrFail($id);

		$users = $event->followers()->get();

		$this->validator($request->all())->validate();

		$job = (new ProcessNotifyFollowersOnMessage($users, $event, $request['message']));

		dispatch($job);

		return redirect('/event/'.$event->id)->with('success', 'Mensagem enviada com Sucesso!');
	}

	protected function validator(array $request)
	{   

		return Validator::make($request, $this->rules($request), $this->messages($request));

	}

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules($request)
    {       

    	$rules = array (
    		'message' => 'required|between:10,1000',
    		);

    	return $rules;
    }


        /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
        public function messages($request)
        {

        	$messages = array (

        		'message.required' => 'É necessário definir a mensagem que pretende enviar!',

        		'message.between' => 'A mensagem fornecida deverá estar compreendida entre :min e :max caracteres!'

        		);


        	return $messages;
        }

    }
