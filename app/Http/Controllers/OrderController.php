<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Ticket;
use App\Event;
use Illuminate\Support\Facades\Validator;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Mail;
use App\Mail\TicketEmail;

class OrderController extends Controller
{
	public function store (Request $request, $id){
		$this->validator($request->all())->validate();

		$event = Event::findOrFail($id);

		$ticketTypeIds = $event->ticketTypes()->pluck('id');

		$ticketTypes = $event->ticketTypes();

		$quantityErrors = $this->checkRemainingTicketQuantities($request->input('ticketQuantities'), $ticketTypes);
		
		if(sizeof($quantityErrors['unsuficientTickets']) > 0)
		{
			return response()->json($quantityErrors,422);
		}

		if($event){

			$order = Order::create(['ammount' => $request->input('totalAmmount'),
				'event_id' => $id, 'user_id' => !is_null($request->user()) ? $request->user()->id : null, 'tickets' => sizeof($request->input('tickets'))]);

			$count = Ticket::whereIn('ticket_type_id', $ticketTypeIds)->count();

			$count ++;

			$ticketsFiles = [];

			foreach ($request->input('tickets') as $key => $value) {
				if(in_array($value['ticketTypeId'], $ticketTypeIds->all()))
				{
					$ticket = Ticket::create(['ticket_type_id' => $value['ticketTypeId'], 'order_id' => $order->id, 'email' => isset($value['email']) ? $value['email'] : $request['defaultEmail'], 'owner_name' => $value['ownerName'], 'identification_number' => $event->id . '/' . $count, 'transmissable' => $value['transmissable'], 'private_code' => str_random(4)]);

					$file = $this->generateTicketPDF($ticket, $event);

					array_push($ticketsFiles, $file);

					if(isset($value['email'])){
						Mail::to($value['email'])->send(new TicketEmail($file));
					}
				}

				$count ++;

			}

			Mail::to($request['defaultEmail'])->send(new TicketEmail($ticketsFiles));

		}
		else{
			return response()->json('No Event With such ID', 405);
		}

		return;


	}

	private function checkRemainingTicketQuantities($ticketQuantities, $ticketTypesQuantities, $ticketTypeIds)
	{
		//return the totalBought
		$counters = Ticket::whereIn('ticket_type_id', $ticketTypeIds)
		->select('ticket_type_id', DB::raw('count(*) as totalBought'))
		->groupBy('ticket_type_id')
		->get();


		foreach ($ticketTypesQuantities as $key => $value) {

			for ($i=0; $i < sizeof($counters) ; $i++) { 

				if($value->id == $counters[$i]->ticket_type_id)
				{
					$counters[$i]['remaining'] = $value->quantity - $counters[$i]->totalBought;
				}
			}

		}

		$errorBag = [];

		$errorBag['unsuficientTickets'] = array();

		foreach ($ticketQuantities as $key => $value) {

			if($value['bought'] > 0)
			{
				for ($i=0; $i < sizeof($counters); $i++) {

					if($value['ticketTypeId'] == $counters[$i]->ticket_type_id )
					{	
						if($counters[$i]['remaining'] == 0)
						{
							array_push($errorBag['unsuficientTickets'], $value['name']);

						}
						else{
							if($counters[$i]['remaining'] - $value['bought'] < 0)
							{
								array_push($errorBag['unsuficientTickets'], $value['name']);
							}
						}

					}
				}
			}

		}

		return $errorBag;
	}

	public function generateTicketPDF($ticket, $event)
	{
		$pdf = new Fpdf();

		$pdf->AddPage();
		$pdf->SetMargins(2, 2);

		$pdf->Image(storage_path('/app/public/event/'.$event->image_id), null, null, 70);

		$pdf->SetXY(84, 10);
		$pdf->SetFont('Helvetica', 'BI', 20);
		$pdf->Write(6, substr($event->name, 0, 29));
		if(strlen($event->name) > 30) {
			$pdf->SetXY(84, 17);
			$pdf->Write(5, substr($event->name, 30, 47));
		}

		$pdf->Ln();
		$pdf->SetFont('Helvetica', null, 14);
		$pdf->SetTextColor(0, 0, 0);

		$pdf->SetXY(84, 25);
		$pdf->Write(10, $ticket->ticketType->name);

		$pdf->SetXY(145, 22);
		$pdf->SetFont('Helvetica', 'BI', 20);
		$pdf->Write(10, "ID: ".$ticket->identification_number);

		if($event->evalType == "conditional") {
			$pdf->SetXY(145, 30);
			$pdf->SetFont('Helvetica', 'I', 14);
			$pdf->Write(5, iconv('UTF-8', 'windows-1252', "Cod.: ")."".$ticket->private_code);
		}

		$pdf->SetFont('Helvetica', 'U', 14);
		if(!is_null($ticket->owner_name)) {
			$pdf->SetXY(84, 34);
			$pdf->Write(10, iconv('UTF-8', 'windows-1252', "Identificação:"));

			$pdf->SetFont('Helvetica', null, 14);
			$pdf->Write(10, iconv('UTF-8', 'windows-1252', " ".$ticket->owner_name));
		}

		$pdf->SetXY(84, 41);
		$pdf->Write(10, iconv('UTF-8', 'windows-1252', $event->getDate($event->initDate)));

		$pdf->SetXY(84, 48);
		$pdf->SetFont('Helvetica', 'U', 14);
		$pdf->Write(10, "Latitude:");
		$pdf->SetFont('Helvetica', null, 14);
		$pdf->Write(10, " ".$event->lat." ");

		$pdf->SetFont('Helvetica', 'U', 14);
		$pdf->Write(10, "Longitude:");
		$pdf->SetFont('Helvetica', null, 14);
		$pdf->Write(10, " ".$event->lng);

		$pdf->SetXY(84, 55);
		$pdf->SetFont('Helvetica', 'U', 14);
		$pdf->Write(10, "Distrito:");

		$pdf->SetFont('Helvetica', null, 14);
		$pdf->Write(10, " ".iconv('UTF-8', 'windows-1252', $event->district->name));

		$pdf->SetXY(160, 55);
		$pdf->Write(10, $ticket->ticketType->price." Euros");
		$pdf->Ln();

		$filename = str_replace('/', '_', $ticket->identification_number);
		$filename = $filename.".pdf";

		$pdf->Output('F', storage_path('/app/public/tickets/'.$filename));

		return $filename;
	}

	/*
    * @param  array  $data
    * @return \Illuminate\Contracts\Validation\Validator
    */

	protected function validator(array $payload)
	{   
		return Validator::make($payload, $this->rules($payload), $this->messages($payload));
	}

	/**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
	public function rules($payload)
	{       
		$rules = [
		'defaultEmail' => 'required|email|max:200',
		];

		foreach ($payload['tickets'] as $key => $value) {
			if(isset($value['email']))
			{
				$rules['tickets.'.$key.'.email'] = 'required|email|max:200';
			}
		}

		return $rules;
	}


	/**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
	public function messages($payload)
	{
		$messages = array (
			'defaultEmail.required' => 'É necessário definir o email por definição',
			'defaultEmail.email' => 'O email fornecido deverá ser um email válido',
			'defaultEmail.max' => 'O email fornecido deverá ter um tamanho máximo de :max carateres',
			'tickets.*.email.email' => 'O email fornecido deverá ser um email válido',
			'tickets.*.email.required' => 'O preenchimento do campo email de envio é obrigatório',
			'tickets.*.email.max' => 'O email fornecido deverá ter um tamanho máximo de :max carateres',

			);
		return $messages;
	}
}
