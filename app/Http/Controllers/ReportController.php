<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Report;
use App\Event;

class ReportController extends Controller
{
	public function report($id)
	{
		$event = Event::findOrFail($id);

		return view('event.report', compact('event'));
	}

	public function saveReport(Request $request, $id)
	{
		$errors = $this->validator($request->all())->validate();

		if(count($errors) > 0)
		{
			return render_view('event.report', compact('errors'));
		}

		$report = $this->create($request->all(), $id);

		$report->save();

		return redirect('/event/'. $id)->with('success', 'Evento denunciado com Sucesso!');
	}

	public function validator(array $data)
	{
		$rules = array (
			'reason' => 'required|max:1000');

		$messages = array (
			'required' => 'O campo Razão de Denúncia é de preenchimento obrigatório',
			'max' => 'O campo Razão de Denúncia poderá ter no maximo :max carateres');

		return Validator::make($data, $rules, $messages);
	}

	public function create(array $data, $id)
	{
		return Report::create([
			'event_id' => $id,
			'reason' => $data['reason'],
			]);
	}

	public function show($id)
	{
		$currentView = "";

		$report = Report::findOrFail($id);

		return view('administrator.events.reports.show', compact('currentView', 'report'));
	}

	public function setReviewed($id)
	{
		$report = Report::findOrFail($id);

		$report->reviewed = true;

		$report->save();

		return redirect()->back();
	}
}
