<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\District;
use Intervention\Image\ImageManagerStatic as Image;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::select('id', 'name', 'initDate', 'district_id', 'image_id')->where('status', 1)->where('hidden', 0)->inRandomOrder()->take(6)->get();

        foreach ($events as $event) {
            $event['district_id'] = District::select('name')->findOrFail($event['district_id']);

            $event['image'] = Image::make(Storage::disk('local')->get('public/event_list/' . $event['image_id']))->encode('data-url')->encoded;
        }
        

        return view('home', compact('events'));
    }
}
