<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class NotAdminAndAlreadyEvaluate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Auth::guest()) {
            if($request->user()->evaluations()->where('event_id', $request->route()->id)->count() > 0 || $request->user()->isAdmin() == 1)
            {
                return redirect('event/'.$request->route()->id);   
            }

        }
        return $next($request);
    }
}
