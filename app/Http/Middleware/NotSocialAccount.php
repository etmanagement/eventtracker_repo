<?php

namespace App\Http\Middleware;

use Closure;

class NotSocialAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->social_account) 
        {
            return redirect('/user/personal/profile'); 
        }

        return $next($request);
    }
}
