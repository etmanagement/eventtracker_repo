<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Auth;


use Closure;

class HiddenEvent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * */
    public function handle($request,  Closure $next)
    {


        $event = Event::findOrFail($request->route()->id);
        $user = $request->user();

        
        if($event->hidden){

            if(is_null($user))
            {
                return redirect()->route('home');
            }

            if($event->user_id == $user->id || $user->isAdmin())
            {   
                return $next($request);
            }
            else{
                return redirect()->route('home');
            }
        }

        return $next($request);
        
    }
}
