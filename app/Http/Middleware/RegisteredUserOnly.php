<?php

namespace App\Http\Middleware;

use Closure;

class RegisteredUserOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() == null || $request->user()->isAdmin() == 1)
        {
            return redirect('home');   
        }

        return $next($request);
    }
}
