<?php

namespace App\Http\Middleware;

use Closure;
use App\Event;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class TicketManagementET
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $event = Event::findOrFail($request->route()->id);
        $user = Auth::guest() ? null : Auth::user();
        if($event->ticketManagement == 'eventTracker')
        {
            if(Carbon::parse($event->initDate) < Carbon::now())
            {
                if(is_null($user) ? true : $user->id != $event->id)
                {
                    return redirect(route('home'));
                }

            }

            return $next($request);
        }


        return redirect(route('home'));
    }
}
