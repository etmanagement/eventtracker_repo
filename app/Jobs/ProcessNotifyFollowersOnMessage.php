<?php

namespace App\Jobs;

use App\Event;
use App\Mail\PromotorMessage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessNotifyFollowersOnMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $users;
    protected $event;
    protected $message;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $users, Event $event, $message)
    {
        $this->users = $users;
        $this->event = $event;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        foreach ($this->users as $user) {
            Mail::to($user)->queue(new PromotorMessage($user, $this->event, $this->message));
        }
    }
}
