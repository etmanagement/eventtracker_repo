<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = true;

	protected $table = 'orders';
	
	protected $fillable = [
		'ammount', 'event_id', 'user_id', 'tickets'
	];

	public function tickets()
 	{
 		return $this->hasMany('App\Ticket');
 	}

 	public function event()
 	{
 		return $this->belongsTo('App\Event');
 	}
}
