<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class CreateAccountToMergeWithSocialAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $email )
    {
        $this->email = $email;
        $user = User::where('email', $email)->first();
        $this->url = url('/saveRegistedPassword', $user->email_token);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Criação de Conta associada a Rede Social!')->markdown('emails.create-account-to-merge');
    }
}
