<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $ticketsFiles;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ticketsFiles)
    {
        $this->ticketsFiles = $ticketsFiles;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sendEmail = $this->subject('Bilhetes');

        if(is_array($this->ticketsFiles))
        {
            foreach ($this->ticketsFiles as $ticketFile) {
                $sendEmail->attach(storage_path('app/public/tickets/'.$ticketFile));
            }
        } else {
            $sendEmail->attach(storage_path('app/public/tickets/'.$this->ticketsFiles));
        }

        $sendEmail->markdown('emails.ticket-email');

        return $sendEmail; 
    }
}
