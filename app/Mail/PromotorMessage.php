<?php

namespace App\Mail;

use App\User;
use App\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PromotorMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $event;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Event $event, $message)
    {
        $this->user = $user;
        $this->event = $event;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->event->name)->markdown('emails.promotor-message');
    }
}
