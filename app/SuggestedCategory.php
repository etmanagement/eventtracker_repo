<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuggestedCategory extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'suggested_categories';

    protected $fillable = [
    'name', 'event_id'
    ];

    public function event(){

        return $this->belongsTo('Event');
        
    }
}
