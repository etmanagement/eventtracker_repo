<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    public $timestamps = true;

    protected $table = 'events';

    /* Quick notes on event->status:
    *	1 - Status = Advertised
    *   2 - Status = In Cancellation 
    *   3 - Status = Canceled
    *	4 - Status = In Eval
    *	5 - Status = Finished
    *	
    */

    protected $fillable = [
    'status', 'name', 'initDate', 'finishDate', 'description', 'spaceName', 'disctrict',
    'address' , 'lat', 'lng', 'hasTickets', 'ticketsForSale', 'ticketManagement',
    'ticketSaleDescription', 'evalType', 'evalTime', 'finalEvalDay', 'district_id', 'category_id', 'imageID', 'user_id',
    ];

    public function getDate($date) {
        $dt = Carbon::parse($date);

        $weekday = array();
        $weekday[0] = "Domingo";
        $weekday[1] = "Segunda-Feira";
        $weekday[2] = "Terça-Feira";
        $weekday[3] = "Quarta-Feira";
        $weekday[4] = "Quinta-Feira";
        $weekday[5] = "Sexta-Feira";
        $weekday[6] = "Sabado";

        $month = array();
        $month[0]="Janeiro";
        $month[1]="Fevereiro";
        $month[2]="Março";
        $month[3]="Abril";
        $month[4]="Maio";
        $month[5]="Junho";
        $month[6]="Julho";
        $month[7]="Agosto";
        $month[8]="Setembro";
        $month[9]="Outubro";
        $month[10]="Novembro";
        $month[11]="Dezembro";

        $dateString = $weekday[$dt->dayOfWeek].", ".($dt->day < 10 ? "0".$dt->day : $dt->day)." de ".$month[($dt->month-1)]." de ".($dt->year);

        return $dateString;

    }

    public function statusToStr() {
        $date = Carbon::now();

        $statusStr = "";
        switch ($this->status) {
            case 1:
            if($this->initDate <= $date->toDateTimeString()) {
                $statusStr = "A decorrer";
            } else {
                $statusStr = "Publicitado";
            }
            break;
            case 2:
            $statusStr = "Em Cancelamento";
            break;
            case 3:
            $statusStr = "Cancelado";
            break;
            case 4:
            $statusStr = "Terminado";
            break;
        }

        return $statusStr;
    }


    public function ticketsStartingAt(){

        $min = 0;

        foreach ($this->ticketTypes()->get() as $key => $value) {
            if($value->is_standard)
            {   
                return 0;
            }    
            else{
                if($key != 0)
                {
                    if($value->price < $min )
                    {
                        $min = $value->price;
                    }
                }
                else{
                    $min = $value->price;
                }
            }

        }


        return $min;
    }

    public function isOwner($user)
    {
        if(is_null($user))
        {
            return true;
        }
        else{
            if($user->id == $this->id)
            {
                return false;

            }
            return true;
        }

    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function district() {
        return $this->belongsTo('App\District');
    }

    public function reports() {
        return $this->hasMany('App\Report');
    }

    public function multimediaResources(){

        return $this->hasMany('App\Multimedia');
    }

    public function ticketSalesPoints(){
        return $this->hasMany('App\TicketSalesPoint');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function suggestedCategory(){
        return $this->hasOne('App\SuggestedCategory');
    }

    public function cancellation(){
        return $this->hasOne('App\Cancellation');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'event_followers');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }

    public function usersEvaluation()
    {
        return $this->hasMany('App\UserEvaluation');
    }

    public function orders() 
    {
        return $this->hasMany('App\Order');
    }

    public function ticketTypes(){
        return $this->hasMany('App\TicketType');
    }

    public function initDateHasPassed(){

        return Carbon::now() > Carbon::parse($this->initDate);
    }
}
