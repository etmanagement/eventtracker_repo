<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	public $timestamps = false;

	protected $table = 'tags';

	protected $fillable = array('tag', 'search_tag');

	public function events()
	{
		return $this->belongsToMany('App\Event');
	}
}
