<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketSalesPoint extends Model
{
    /**
    * Indicates if the model should be timestamped.
    *
    * @var bool
    */
    public $timestamps = false;

    protected $table = 'ticket_sales_points';

    protected $fillable = array('url', 'event_id');

    public function event(){

    	return $this->belongsTo('Event');
    	
    }
}
