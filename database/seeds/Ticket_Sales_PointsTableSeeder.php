<?php

use Illuminate\Database\Seeder;

class Ticket_Sales_PointsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = App\Event::where('ticketManagement', 'external')->get()->toArray();

        $faker = Faker\Factory::create('pt_PT');

        $this->command->info('Creating ticket sales points and assigning to events ...');
        $bar = $this->command->getOutput()->createProgressBar(count($events));
        for ($i = 0; $i < count($events); ++$i) {
            DB::table('ticket_sales_points')->insert($this->fakeTicketSalesPoint($faker, $events[$i]['id']));
            $bar->advance();
        }
        $bar->finish();
        $this->command->info('');
    }

    public function fakeTicketSalesPoint(Faker\Generator $faker, $event_id)
    {
        return [
            'url' => $faker->url,
            'event_id' => $event_id
        ];
    }
}
