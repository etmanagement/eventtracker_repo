<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$districts[0]['name'] = 'Não Definido';
    	$districts[0]['country'] = 'all';

    	$districts[1]['name'] = 'Aveiro';
    	$districts[1]['country'] = 'pt';

    	$districts[2]['name'] = 'Beja';
    	$districts[2]['country'] = 'pt';

    	$districts[3]['name'] = 'Braga';
    	$districts[3]['country'] = 'pt';

    	$districts[4]['name'] = 'Bragança';
    	$districts[4]['country'] = 'pt';

    	$districts[5]['name'] = 'Castelo Branco';
    	$districts[5]['country'] = 'pt';

    	$districts[6]['name'] = 'Coimbra';
    	$districts[6]['country'] = 'pt';

    	$districts[7]['name'] = 'Évora';
    	$districts[7]['country'] = 'pt';

    	$districts[8]['name'] = 'Faro';
    	$districts[8]['country'] = 'pt';

    	$districts[9]['name'] = 'Guarda';
    	$districts[9]['country'] = 'pt';

    	$districts[10]['name'] = 'Leiria';
    	$districts[10]['country'] = 'pt';

    	$districts[11]['name'] = 'Lisboa';
    	$districts[11]['country'] = 'pt';

    	$districts[12]['name'] = 'Portalegre';
    	$districts[12]['country'] = 'pt';

    	$districts[13]['name'] = 'Porto';
    	$districts[13]['country'] = 'pt';

    	$districts[14]['name'] = 'Santarém';
    	$districts[14]['country'] = 'pt';

    	$districts[15]['name'] = 'Setúbal';
    	$districts[15]['country'] = 'pt';

    	$districts[16]['name'] = 'Viana do Castelo';
    	$districts[16]['country'] = 'pt';

    	$districts[17]['name'] = 'Vila Real';
    	$districts[17]['country'] = 'pt';

    	$districts[18]['name'] = 'Viseu';
    	$districts[18]['country'] = 'pt';

    	$districts[19]['name'] = 'Açores';
    	$districts[19]['country'] = 'pt';

    	$districts[20]['name'] = 'Madeira';
    	$districts[20]['country'] = 'pt';

        $this->command->info('Creating districts ...');
    	for ($i=0; $i < 21; $i++) { 
    		DB::table('districts')->insert($districts[$i]);
    	}
        $this->command->info('');

    }
}
