<?php

use Illuminate\Database\Seeder;

class CancellationsTableSeeder extends Seeder
{

	/**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
	{
		$canceledEvents = App\Event::whereIn('status', [2, 3])->get()->toArray();

		$faker = Faker\Factory::create('pt_PT');

		$this->command->info('Creating '.count($canceledEvents).' cancellations ...');
		$bar = $this->command->getOutput()->createProgressBar(count($canceledEvents));
		for ($i = 0; $i < count($canceledEvents); ++$i) {
			DB::table('cancellations')->insert($this->fakeCancellation($faker, $canceledEvents[$i]));
			$bar->advance();
		}
		$bar->finish();
        $this->command->info('');
	}

	private function fakeCancellation(Faker\Generator $faker, $event) 
	{
		$createdAt = $faker->dateTimeBetween($event['created_at'], $event['initDate']);

		$finalCancelDay = Carbon\Carbon::parse($createdAt->format('Y-m-d H:i:s'))->addWeeks(2);

		$finalCancelDay = $finalCancelDay > $event['finishDate'] ? $event['finishDate'] : $finalCancelDay;

		return [
		'created_at' => $createdAt,
		'updated_at' => $createdAt,
		'final_cancel_day' => $finalCancelDay,
		'reason' => $faker->realText,
		'event_id' => $event['id']
		];
	}
}