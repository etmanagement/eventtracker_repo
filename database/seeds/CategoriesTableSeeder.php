<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories[0]['categoryName'] = 'Não Definida';
        $categories[1]['categoryName'] = 'Ação de Sensibilização';
        $categories[2]['categoryName'] = 'Cinema Amador';
        $categories[3]['categoryName'] = 'Cultura Contemporanêa';
        $categories[4]['categoryName'] = 'Desportos Ao Ar Livre';
        $categories[5]['categoryName'] = 'Desportos Indoor';
        $categories[6]['categoryName'] = 'Entretenimento Noturno';
        $categories[7]['categoryName'] = 'Evento Sazonal';
        $categories[8]['categoryName'] = 'Exposição de  Produtos';
        $categories[9]['categoryName'] = 'Festival / Arraial';
        $categories[10]['categoryName'] = 'Formação Académica';
        $categories[11]['categoryName'] = 'Formação Profissional';
        $categories[12]['categoryName'] = 'Gastronomia';
        $categories[13]['categoryName'] = 'Inovação';
        $categories[14]['categoryName'] = 'Jogos de Computador / Gaming';
        $categories[15]['categoryName'] = 'Música Ao Vivo';
        $categories[16]['categoryName'] = 'Palestra / Seminário';
        $categories[17]['categoryName'] = 'Passeio Motorizado';
        $categories[18]['categoryName'] = 'Passeio Pedestre';
        $categories[19]['categoryName'] = 'Recriação de Periodos Históricos';
        $categories[20]['categoryName'] = 'Teatro e Atuações';
        $categories[21]['categoryName'] = 'Tecnologia';
        $categories[22]['categoryName'] = 'Workshop';
        $categories[23]['categoryName'] = 'Outra (...)';

        $this->command->info('Creating categories ...');
        for ($i=0; $i < 24; $i++) { 
        	DB::table('categories')->insert($categories[$i]);
        }
        $this->command->info('');

    }
}
