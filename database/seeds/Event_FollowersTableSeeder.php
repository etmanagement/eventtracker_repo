<?php

use Illuminate\Database\Seeder;

class Event_FollowersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = DB::table('events')->pluck('id')->toArray();
        $users = DB::table('users')->pluck('id')->toArray();

		$faker = Faker\Factory::create('pt_PT');

		$this->command->info('Adding followers to events ...');
		$bar = $this->command->getOutput()->createProgressBar(count($events));
		for ($i = 0; $i < count($events); ++$i) {
			$numberUsers = $faker->numberBetween(1, count($users));
			$numberUsers = (int) $numberUsers/$faker->numberBetween(1, $numberUsers);

			$usersUsed = array();
			for($j = 0 ; $j < $numberUsers ; $j++) {
				$user = $users[$faker->numberBetween(0, count($users)-1)];
				if(!in_array($user, $usersUsed)) {
					DB::table('event_followers')->insert(['user_id' => $user, 'event_id' => $events[$i]]);
					array_push($usersUsed, $user);
				} else {
					$j--;
				}
			}
			$bar->advance();
		}
		$bar->finish();
    	$this->command->info('');
    }
}
