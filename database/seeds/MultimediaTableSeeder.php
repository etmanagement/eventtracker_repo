<?php

use Illuminate\Database\Seeder;

class MultimediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = DB::table('events')->pluck('id')->toArray();

		$faker = Faker\Factory::create('pt_PT');

		$this->command->info('Creating multimedia for events ...');
		$bar = $this->command->getOutput()->createProgressBar(count($events));
		for ($i = 0; $i < count($events); ++$i) {
			$numberMulti = $faker->numberBetween(0, 3);

			for($j = 0 ; $j < $numberMulti ; $j++) {
				DB::table('multimedia')->insert($this->fakeMultimedia($faker, $events[$i]));
			}
			$bar->advance();
		}
		$bar->finish();
    	$this->command->info('');
    }

    private function fakeMultimedia(Faker\Generator $faker, $event_id)
    {
    	return [
    		'name' => $faker->name,
    		'url' => $faker->url,
    		'event_id' => $event_id
    	];
    }
}
