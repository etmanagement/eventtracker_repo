<?php

use Illuminate\Database\Seeder;

class ReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$events = App\Event::all()->toArray();

    	$faker = Faker\Factory::create('pt_PT');

    	$this->command->info('Adding reports to some events ...');
    	$bar = $this->command->getOutput()->createProgressBar(count($events));
    	for ($i = 0; $i < count($events); ++$i) {
    		if($faker->numberBetween(0,1) == 1) {
    			if($faker->numberBetween(0,1) == 1) {
    				$numberReports = $faker->numberBetween(0, $faker->numberBetween(1, 8));
    				for($j = 0 ; $j < $numberReports ; $j++) {
    					DB::table('reports')->insert($this->fakeReport($faker, $events[$i]['id']));
    				} 
    			}
    		}
    		$bar->advance();
    	}
    	$bar->finish();
    	$this->command->info('');
    }

    private function fakeReport(Faker\Generator $faker, $event_id) 
    {
    	$createdAt = Carbon\Carbon::now()->subDays($faker->numberBetween(1,20));

    	return [
    	'event_id' => $event_id,
    	'reason' => $faker->realText,
    	'reviewed' => $faker->randomElement([true, false]),
    	'created_at' => $createdAt,
    	'updated_at' => $createdAt
    	];

    }
}
