<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	private $photoPath = 'public/profile';
    private $numberOfUsers = 20;
    private $numberOfBlockedUsers = 5;
	private $numberOfImages = 10;


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->table(['Users table seeder notice'], [
            ['Profile photos will be stored on path '.storage_path('app/'.$this->photoPath)],
            ['A progress bar is displayed because photos will be downloaded from lorempixel'],
            ['Edit this file to change the storage path or the number of users']
        ]);

        if ($this->command->confirm('Do you wish to delete photos from '.storage_path('app/'.$this->photoPath).'?', true)) {
            Storage::deleteDirectory($this->photoPath);
        }
        Storage::makeDirectory($this->photoPath);

        $faker = Faker\Factory::create('pt_PT');

        $this->command->info('Downloading Images for users...');
    	$images = $this->getImages($faker);
    	$this->command->info('');

        $this->command->info('Creating '.$this->numberOfUsers.' active users...');

        $districts = DB::table('districts')->pluck('id')->toArray();

        $bar = $this->command->getOutput()->createProgressBar($this->numberOfUsers);
        for ($i = 0; $i < $this->numberOfUsers; ++$i) {
            DB::table('users')->insert($this->fakeUser($faker, $faker->randomElement($districts), $faker->randomElement($images)));
            $bar->advance();
        }
        $bar->finish();
        $this->command->info('');

        $this->command->info('Creating '.$this->numberOfBlockedUsers.' blocked users...');
        $bar = $this->command->getOutput()->createProgressBar($this->numberOfBlockedUsers);
        for ($i = 0; $i < $this->numberOfBlockedUsers; ++$i) {
            $user = $this->fakeUser($faker, $faker->randomElement($districts), $faker->randomElement($images));
            $user['blocked'] = true;
            DB::table('users')->insert($user);
            $bar->advance();
        }
        $bar->finish();
        $this->command->info('');

        $user = array();
        $user['name'] = 'Administrator';
        $user['email'] = 'admin@email.pt';
        $user['password'] = bcrypt('admin123');
        $user['is_admin'] = true;
        DB::table('users')->insert($user);

        copy(storage_path('app/public/default.jpg'), storage_path('app/'.$this->photoPath.'/default.jpg'));
    }


    private function fakeUser(Faker\Generator $faker, $district_id, $image_id)
    {
        static $password;
        $createdAt = Carbon\Carbon::now()->subDays($faker->numberBetween(0, 10));
        $updatedAt = $faker->dateTimeBetween($createdAt);

        $birthdate = $faker->dateTimeBetween('-80 years', '-14 years', date_default_timezone_get());
        return [
            'name' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'password' => $password ?: $password = bcrypt('user123'),
            'address' => $faker->randomElement([null, $faker->address]),
            'NIF' => $faker->randomElement([null, $faker->numberBetween(100000000, 999999999)]),
            'gender' => $faker->randomElement(['M', 'F', 'O', null]),
            'birthdate' => $faker->randomElement([null, $birthdate]),
            'image_id' => $faker->randomElement(['default.jpg', $image_id, $image_id]),
            'district_id' => $district_id,
            'description' => $faker->randomElement([null, $faker->realText(300)]),
            'remember_token' => str_random(10),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
            'is_admin' => false,
            'social_account' => false,
            'blocked' => false
        ];
    }

    private function newUserImage(Faker\Generator $faker)
    {
    	$imageUrl = $faker->imageUrl(400, 400, 'people', false);

    	$filename = time().'.png';

    	try{

    		$image = Image::make($imageUrl);
    		$image->save(storage_path('/app/public/profile/' . $filename), 100);
    	} catch(Exception $e) {
    		return "default.jpg";
    	}
    	
    	return $filename;

    }

    private function getImages(Faker\Generator $faker)
    {
    	$photoNames = array();

    	$bar = $this->command->getOutput()->createProgressBar($this->numberOfImages);
    	for ($i=0; $i < $this->numberOfImages; $i++) { 
    		array_push($photoNames, $this->newUserImage($faker));
    		$bar->advance();
    	}
    	$bar->finish();

    	return $photoNames;
    }
}
   

