<?php

$factory->define(App\Event::class, function (Faker\Generator $faker) {
    
    return [
    	'status' => $faker->numberBetween(1,4),
    	'name' => $faker->name,
    	'initDate' => $faker->date('Y-m-d H:i:s'),
    	'finishDate' => $faker->date('Y-m-d H:i:s'),
    	'description' => $faker->text,
    	'spaceName' => $faker->state,
    	'address' => $faker->streetAddress,
    	'lat' => $faker->latitude,
    	'lng' => $faker->longitude,
    	'hasTickets' => 0,
    	'evalType' => $faker->randomElement(['conditional', 'free']),
        'evalTime' => $faker->randomElement(['7', '14', '21', '1M']),
    	'finalEvalDay' => $faker->date('Y-m-d H:i:s'),
    	'district_id' => $faker->numberBetween(2,21),
    	'category_id' => $faker->numberBetween(2,24),
    	'user_id' => 1,
    ];
});
