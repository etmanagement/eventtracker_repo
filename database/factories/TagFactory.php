<?php

$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    
    return [
        'tag' => $faker->word,
        'search_tag' => $faker->word,
        'counter' => $faker->numberBetween(0, 100),
    ];
});
