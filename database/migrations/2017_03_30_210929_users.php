<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name', 75);
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('address', 100)->nullable();
            $table->string('NIF', 9)->unique()->nullable();
            $table->string('gender', 1)->nullable();
            $table->dateTime('birthdate')->nullable();
            $table->string('image_id', 38)->default('default.jpg');
            $table->integer('district_id')->unsigned()->default(1);
            $table->string('description', 1000)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->boolean('is_admin')->default(0);
            $table->unsignedTinyInteger('social_account')->default(0);
            $table->string('email_token')->nullable();
            $table->boolean('blocked')->default(0);
            $table->boolean('confirmed')->default(0);
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
