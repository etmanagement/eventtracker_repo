<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'facebook' => [
    'client_id' => '1704231253202692',
    'client_secret' => '0b738978e645e7fa4665f714bc897bfe',
    'redirect' => env('APP_URL').'/callback/facebook',
    ],

    'google' => [
    'client_id' => '870427979119-45lal6obuvq4qadqdpm4nrsr2n6cr7u1.apps.googleusercontent.com',
    'client_secret' => 'FezqdWyQlYFKUKVMddKC66LF',
    'redirect' => env('APP_URL').'/callback/google',
    ],

   'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],



];
