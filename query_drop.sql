SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE users;
DROP TABLE events;
DROP TABLE tags;
DROP TABLE password_resets;
DROP TABLE multimedia;
DROP TABLE event_tag;
DROP TABLE categories;
DROP TABLE suggested_categories;
DROP TABLE districts;
DROP TABLE social_accounts;
DROP TABLE ticket_sales_points;
DROP TABLE reports;
DROP TABLE cancellations;
DROP TABLE event_followers;
DROP TABLE evaluations;
DROP TABLE user_evaluations;
DROP TABLE ticket_types;
DROP TABLE jobs;
DROP TABLE failed_jobs;
DROP TABLE facebook_temp;
DROP TABLE orders;
DROP TABLE tickets;
TRUNCATE TABLE migrations;

SET FOREIGN_KEY_CHECKS = 1;